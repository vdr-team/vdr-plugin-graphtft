/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  common.h - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: common.h,v 1.7 2007/12/03 19:58:20 root Exp $
 *
 **/

#ifndef ___COMMON_H
#define ___COMMON_H

#include <string>
#include <iconv.h>
#include <stdint.h>

using std::string;

enum TimeConst
{
   tmeSecondsPerMinute = 60,
   tmeSecondsPerHour = tmeSecondsPerMinute * 60,
   tmeSecondsPerDay = tmeSecondsPerHour *24
};

enum Misc
{
   success = 0,
   done    = success,
   fail    = -1,
   ignore  = -1,
   na      = -1,
   yes     = 1,
   on      = 1,
   off     = 0,
   no      = 0,
   TB      = 1
};

enum Sizes
{
   sizeStamp = 14,
   sizeTime = 6,
   sizeDate = 8,
   sizeHHMM = 4
};

enum LogDevice
{
   devNone,       // 0
   devStdOut,     // 1
   devSyslog,     // 2
   devFile        // 3
};

enum Eloquence
{
   eloOff,               // 0
   eloAlways,            // 1
   eloDetail,            // 2
   eloDebug,             // 3
   eloDebug1 = eloDebug, // 3
   eloDebug2,            // 4
   eloDebug3             // 5
};

extern int logLevel;
extern int logDevice;

#ifndef __FRONTEND
  void tell(int eloquence, const char* format, ...);
#endif

int fileExists(string filename);

int isolat1ToUTF8(unsigned char* out, int *outlen, 
                  const unsigned char* in, int *inlen);

int toUTF8(char* out, int outMax, const char* in);
uint64_t msNow();

//***************************************************************************
// 
//***************************************************************************

int getFrontendSTR();
int getFrontendSNR();

//***************************************************************************
// Wrapper Regual Expression Library
//***************************************************************************

enum Option
{
  repUseRegularExpression = 1,
  repIgnoreCase = 2
};

int rep(const char* string, const char* expression, Option options = repUseRegularExpression);

int rep(const char* string, const char* expression, 
        const char*& s_location, Option options = repUseRegularExpression);

int rep(const char* string, const char* expression, const char*& s_location, 
        const char*& e_location, Option options = repUseRegularExpression);


//***************************************************************************
// Log Duration
//***************************************************************************

class LogDuration
{
   public:

      LogDuration(const char* aMessage, int aLogLevel = 2);
      ~LogDuration();

      void show(const char* label = "");

   protected:

      char message[1000];
      uint64_t durationStart;
      int logLevel;
};

//***************************************************************************
// Class Str
//***************************************************************************

class Str
{
   public:

      // Manipulation

      static char* rTrim(char* buf);
      static char* lTrim(char* buf);
      static char* allTrim(char* buf);

      // converting

      static const char* toStr(const char* s);
      static const char* toStr(bool value);
      static const char* toStr(int value);
      static const char* toStr(double value, int precision = 2);

      // Checks

      static int isEmpty(const char* buf);
      static int isBlank(const char* buf);
      static const char* notNull(const char* s)   { return s ? s : ""; }
};

//***************************************************************************
#endif //___COMMON_H
