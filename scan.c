/*
 * dspitems.c: A plugin for the Video Disk Recorder
 *
 * See the README file for copyright information and how to reach the author.
 *  (c) 2007-2008 J�rg Wendel
 *
 * Date: 09.11.08
 */

//***************************************************************************
// Includes
//***************************************************************************

#include <scan.h>

//***************************************************************************
// Scanner
//***************************************************************************

const char* Scan::delimiters = " ;,(){}[]\"";
const char* Scan::operators  = " +-*:<>!=";
const char* Scan::whitespace = " ()";
const char* Scan::logicalOps = "|&";
const char* Scan::numprefix = "+-";
