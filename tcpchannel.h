//***************************************************************************
// Group VDR/GraphTFT
// File tcpchannel.h
// Date 31.10.06
// This code is distributed under the terms and conditions of the
// GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
// (c) 2006-2008 J�rg Wendel
//--------------------------------------------------------------------------
// Class TcpChannel
//***************************************************************************

#ifndef __GTFT_TCPCHANNEL_HPP__
#define __GTFT_TCPCHANNEL_HPP__

#include <vdr/thread.h>

//***************************************************************************
// Class TcpChannel
//***************************************************************************

class TcpChannel
{
   public:	

     enum Errors
      {
         errChannel = -100,

         errUnknownHostname,
         errBindAddressFailed,
         errAcceptFailed,
         errListenFailed,
         errConnectFailed,
         errIOError,
         errConnectionClosed,
         errInvalidEndpoint,
         errOpenEndpointFailed,

         // Warnungen

         wrnNoEventPending,
         errUnexpectedEvent,
         wrnChannelBlocked,
         wrnNoConnectIndication,
         wrnNoResponseFromServer,
         wrnNoDataAvaileble,
         wrnSysInterrupt,
         wrnTimeout
      };

      struct Header
      {
         int command;
         int size;
      };

		TcpChannel();
		~TcpChannel();

      int open(unsigned short aPort, const char* aLocalHost = 0);
      int close();
		int listen(TcpChannel*& child, int tm = 0);
      int look(int aTimeout = 0);
      int read(char* buf, int bufLen);
      int write(int command, const char* buf = 0, int bufLen = 0);

      int isConnected()    { return client != 0; }

   private:

      int checkErrno();

      // data

      int lookAheadChar;
      int lookAhead;

      int listener;
      int client;
      unsigned short port;   
      long localAddr;
      long nTtlSent;
      long nTtlReceived;
      long timeout;

      cMutex _mutex;
};

//***************************************************************************
#endif // __GTFT_TCPCHANNEL_HPP__
