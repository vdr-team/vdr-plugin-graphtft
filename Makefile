#***************************************************************************
# Group VDR/GraphTFT
# File Makefile
# Date 31.10.06
# This code is distributed under the terms and conditions of the
# GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
# (c) 2006-2008 J�rg Wendel
#***************************************************************************

# ----------------------------------------------------------------------------
# User defined Makefile options for the graphtft plugin
# Change the parameters as necessary.
# ----------------------------------------------------------------------------

FFMDIR = /usr/include

# ----------------------------------------------------------------------------
# like to support a touch screen ?

WITH_TOUCH = 1

# ----------------------------------------------------------------------------
# You will need ffmpeg (libavcodec) for dvb/fb-devices and for softmpeg, 
# so install it ;)

ifeq (exists, $(shell pkg-config libavcodec && echo exists))
  HAVE_AVCODEC = 1
else
  $(warning ******************************************************************)
  $(warning 'libavcodec' not detected! 'HAVE_AVCODEC = 1' will not compiles in)
  $(warning    -> dvb-devices, fb-devices and the X frontend not supported)
  $(warning ******************************************************************)
endif

# ----------------------------------------------------------------------------
# imlib2 needed to enable support for dvb- and fb-devices. 
# imlib2 also needed for graphtft-fe (WITH_X_COMM)

ifeq (exists, $(shell pkg-config imlib2 && echo exists))
  HAVE_IMLIB = 1
  WITH_X_COMM = 1
else
  $(warning ************************************************************************************)
  $(warning 'imlib2' not detected! 'HAVE_IMLIB = 1 + WITH_X_COMM = 1' will not be compiled in...)
  $(warning    -> dvb-devices, fb-devices and the X frontend not supported)
  $(warning ************************************************************************************)
endif

# ----------------------------------------------------------------------------
# ImageMagick.

ifeq (exists, $(shell pkg-config ImageMagick++ && echo exists))
  HAVE_IMAGE_MAGICK = 1
  INCLUDES += $(shell pkg-config --cflags ImageMagick++)
else
  $(warning *********************************************************************************)
  $(warning 'ImageMagick{,++}' not detected! 'HAVE_IMAGE_MAGICK = 1' will not be compiled...)
  $(warning    -> communication wit the x frontend work at a slow rate)
  $(warning *********************************************************************************)
endif

# ----------------------------------------------------------------------------
# Use ffmpeg swscale API instead of deprecated functions.
# Needed for newer ffmpeg versions wich don't supports img_convert any more.

ifeq (exists, $(shell pkg-config libswscale && echo exists))
  HAVE_SWSCALE = 1
else
  $(warning *********************************************************************************)
  $(warning 'libswscale' not found)
  $(warning    -> trying to compile with (depricated) scale method of libavcodec)
  $(warning *********************************************************************************)
endif

# ----------------------------------------------------------------------------
# For direct framebuffer directfb lib is needed

ifeq (exists, $(shell pkg-config directfb && echo exists))
  HAVE_DFB = 1
else
  $(warning *********************************************************************************)
  $(warning 'directfb' not detected! 'HAVE_DFB = 1' will not be compiled...)
  $(warning    -> directfb output not supported)
  $(warning *********************************************************************************)
endif

# ----------------------------------------------------------------------------
# For System Information Sysinfo you need libgtop2

ifeq (exists, $(shell pkg-config libgtop-2.0 && echo exists))
  HAVE_GTOP = 1
else
  $(warning *********************************************************************************)
  $(warning 'libgtop-2.0' not detected! 'HAVE_GTOP = 1' will not be compiled...)
  $(warning    -> most parts of sysinfo not supported)
  $(warning *********************************************************************************)
endif

# ----------------------------------------------------------------------------
# Use fastmemcpy with cpuaccel.
#HAVE_FAST_MEMCPY = 1

# ----------------------------------------------------------------------------
# Fix for using pvr350 framebuffer device.
#HAVE_PVRFB = 1

# ----------------------------------------------------------------------------
# libsoftmpeg needed only for Pbp (not running in the moment!).
#HAVE_SOFTMPEG = 1

# ----------------------------------------------------------------------------
# END of user defined part
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# The name of the plugin.
# This name will be used in the '-P...' option of VDR to load the plugin.
# By default the main source file also carries this name.

PLUGIN = graphtft

### The version number of this plugin (taken from the main source file):

VERSION = $(shell grep 'static const char\* VERSION *=' $(PLUGIN).h | awk '{ print $$6 }' | sed -e 's/[";]//g')

## compiler options:

#CXXFLAGS += -Wextra -pedantic


### The directory environment:

DVBDIR = ../../../../DVB
VDRDIR = ../../..
#VDRDIR = /usr/include/vdr
LIBDIR = ../../lib
TMPDIR = /tmp

# comment out for normal build !!!
VDRDIR = /usr/include/vdr
LIBDIR = .
LOCALEDIR = locale
# !!!


### Allow user defined options to overwrite defaults:

-include $(VDRDIR)/Make.config
-include Make.config

### The version number of VDR (taken from VDR's "config.h"):

APIVERSION = $(shell grep 'define APIVERSION ' $(VDRDIR)/config.h | awk '{ print $$3 }' | sed -e 's/"//g')

### The name of the distribution archive:

ARCHIVE = $(PLUGIN)-$(VERSION)
PACKAGE = vdr-$(ARCHIVE)

### Includes and Defines (add further entries here):

INCLUDES += -I$(VDRDIR)/include -I$(DVBDIR)/include -I. -I./imlibrenderer \
				-I./imlibrenderer/fbrenderer -I./imlibrenderer/dvbrenderer \
				-I./dfbrenderer -I./imlibrenderer/dmyrenderer $(GTOP_INC)

DEFINES += -D_GNU_SOURCE -DPLUGIN_NAME_I18N='"$(PLUGIN)"'
DEFINES += -D__STDC_CONSTANT_MACROS

ifdef HAVE_IMLIB
  LIBS += `imlib2-config --libs`

  ifdef HAVE_IMAGE_MAGICK
    LIBS += `Magick++-config --libs`
    DEFINES += -DHAVE_IMAGE_MAGICK
  endif

  DEFINES += -DHAVE_IMLIB
  HAVE_AVCODEC = 1
endif

ifdef HAVE_GTOP
  GTOP_INC = `pkg-config libgtop-2.0 --cflags`
  GTOP_LIB = `pkg-config libgtop-2.0 --libs`

  DEFINES += -DWITH_SYSINFO
  LIBS += $(GTOP_LIB)
endif

ifdef WITH_X_COMM
  DEFINES += -DWITH_TCP_COM
endif

ifdef WITH_TOUCH
  DEFINES += -DWITH_TOUCH
endif

ifdef HAVE_DFB
  INCLUDES += `directfb-config --cflags`
  LIBS += `directfb-config --libs`
  DEFINES += -DHAVE_DFB
endif

ifdef HAVE_SOFTMPEG
  DEFINES += -DHAVE_SOFTMPEG
  LIBS += -lsoftmpeg
  HAVE_AVCODEC = 1
endif

ifdef HAVE_AVCODEC
  AVCODEC_INC = $(shell pkg-config libavcodec --cflags | sed -e 's/  //g')
  ifeq ($(strip $(AVCODEC_INC)),)
	INCLUDES += -I$(FFMDIR) -I$(FFMDIR)/libavcodec
  else
	INCLUDES += $(AVCODEC_INC)
  endif
  AVCODEC_LIBS = $(shell pkg-config libavcodec --libs)
  ifeq ($(strip $(AVCODEC_LIBS)),)
	LIBS += -lavcodec
  else
	LIBS += $(AVCODEC_LIBS)
  endif

  DEFINES += -DHAVE_FFMPEG

  ifdef HAVE_SWSCALE
    SWSCALE_INC = $(shell pkg-config libswscale --cflags)
    ifeq ($(strip $(SWSCALE_INC)),)
	INCLUDES += -I$(FFMDIR) -I$(FFMDIR)/libswscale
    else
	INCLUDES += $(SWSCALE_INC)
    endif
    SWSCALE_LIBS = $(shell pkg-config libswscale --libs)
    ifeq ($(strip $(SWSCALE_LIBS)),)
	LIBS += -lswscale
    else
	LIBS += $(SWSCALE_LIBS)
    endif
    DEFINES += -DHAVE_SWSCALE
  endif
endif

ifdef HAVE_PVRFB
  DEFINES += -DPVRFB
endif

CXXFLAGS += -g -ggdb -O0 -fPIC

### The object files (add further files here):

COMMONOBJS = $(PLUGIN).o dspitems.o display.o \
	i18n.o setup.o scan.o theme.o common.o sysinfo.o \
   touchthread.o

# transfer.o

ifdef HAVE_FAST_MEMCPY
  COMMONOBJS += memcpy.o cpu_accel.o 
  DEFINES += -DHAVE_FAST_MEMCPY
endif

ifdef HAVE_IMLIB
  IMLIBOBJS =  imlibrenderer/imlibrenderer.o \
					imlibrenderer/fbrenderer/fbrenderer.o \
					tcpchannel.o \
		  			imlibrenderer/dvbrenderer/player.o \
					imlibrenderer/dvbrenderer/dvbrenderer.o \
					imlibrenderer/dvbrenderer/mpeg2encoder.o \
					imlibrenderer/dvbrenderer/quantize.o \
					comthread.o renderer.o
endif

ifdef HAVE_SOFTMPEG
  IMLIBOBJS += imlibrenderer/fbrenderer/mpeg2decoder.o
endif


ifdef HAVE_DFB
  DFBOBJS = dfbrenderer/dfbrenderer.o dfbrenderer/cache.o
endif

### Implicit rules:

%.o: %.c
	$(CXX) $(CXXFLAGS) -c $(DEFINES) $(INCLUDES) -o $@ $<

# Dependencies:

MAKEDEP = g++ -MM -MG
DEPFILE = .dependencies
$(DEPFILE): Makefile
	@$(MAKEDEP) $(DEFINES) $(INCLUDES) $(COMMONOBJS:%.o=%.c) $(IMLIBOBJS:%.o=%.c) $(DFBOBJS:%.o=%.c) > $@

-include $(DEPFILE)

### Internationalization (I18N):

PODIR     = po
LOCALEDIR = $(VDRDIR)/locale
I18Npo    = $(wildcard $(PODIR)/*.po)
I18Nmo    = $(addsuffix .mo, $(foreach file, $(I18Npo), $(basename $(file))))
I18Ndirs  = $(notdir $(foreach file, $(I18Npo), $(basename $(file))))
I18Npot   = $(PODIR)/$(PLUGIN).pot

%.mo: %.po
	msgfmt -c -o $@ $<

$(I18Npot): $(wildcard *.c)
	xgettext -C -cTRANSLATORS --no-wrap -F -k -ktr -ktrNOOP --msgid-bugs-address='<vdr@jwendel.de>' -o $@ $(wildcard *.c)

$(I18Npo): $(I18Npot)
	msgmerge -U --no-wrap -F --backup=none -q $@ $<

i18n: $(I18Nmo)
	@mkdir -p $(LOCALEDIR)
	for i in $(I18Ndirs); do\
	    mkdir -p $(LOCALEDIR)/$$i/LC_MESSAGES;\
	    cp $(PODIR)/$$i.mo $(LOCALEDIR)/$$i/LC_MESSAGES/vdr-$(PLUGIN).mo;\
	    done

### Targets:

all: libvdr-$(PLUGIN).so i18n
	@cp libvdr-$(PLUGIN).so $(LIBDIR)/libvdr-$(PLUGIN).so.$(APIVERSION)

alli: libvdr-$(PLUGIN).so i18n

libvdr-$(PLUGIN).so: $(COMMONOBJS) $(IMLIBOBJS) $(DFBOBJS)
	$(CXX) $(CXXFLAGS) -shared $^ $(LIBS) -o $@

dist: clean
	@-rm -rf $(TMPDIR)/$(ARCHIVE)
	@mkdir $(TMPDIR)/$(ARCHIVE)
	@cp -a * $(TMPDIR)/$(ARCHIVE)
	@tar cjf $(PACKAGE).tar.bz2 -C $(TMPDIR) $(ARCHIVE)
	@-rm -rf $(TMPDIR)/$(ARCHIVE)
	@echo Distribution package created as $(PACKAGE).tar.bz2

clean:
	@-rm -f $(PODIR)/*.mo $(PODIR)/*.pot
	@-rm -f $(COMMONOBJS) $(IMLIBOBJS) $(DFBOBJS) $(DEPFILE) *.so *.tar.bz2 core* *~
	@rm -rf .libs test
	@rm -rf plasma/build
	(cd graphtft-fe; make -s clean)

TDEF += -DLOCDIR=\"$(LOCDIR)\"
test: test.c scan.c scan.h imlibrenderer/imlibrenderer.c common.c renderer.c 
	g++ $(INCLUDES) $(CXXFLAGS) $(TDEF) $(LIBS) imlibrenderer/imlibrenderer.c \
		 renderer.c common.c scan.c test.c \
       ~/vdr/tools.c \
       ~/vdr/i18n.c \
       ~/vdr/thread.c \
		 -lrt -o $@

libtoptest: cpuload-test.c sysinfo.c sysinfo.h common.h
	g++ cpuload-test.c  sysinfo.c  -I. `pkg-config libgtop-2.0 --cflags` `pkg-config libgtop-2.0 --libs` -DWITH_SYSINFO -DWITHOUT_VDR -o $@
