/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  osd.c - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: osd.c,v 1.5 2007/12/03 19:58:20 root Exp $
 *
 **/

//***************************************************************************
// Include
//***************************************************************************

#include "common.h"
#include "display.h"
#include "osd.h"
#include "setup.h"

//***************************************************************************
// Object
//***************************************************************************

cGraphTFTOsdObject::cGraphTFTOsdObject(cGraphTFTDisplay* display)
{
   _display = display;
   osd = 0;
}

cGraphTFTOsdObject::~cGraphTFTOsdObject()
{
   if (osd) delete osd;
}

//***************************************************************************
// Show
//***************************************************************************

void cGraphTFTOsdObject::Show()
{
   tell(1,"osd.c: Show()");

   osd = cOsdProvider::NewOsd(720, 576);

   if (osd)
   {
      eOsdError status;
      const cFont* font = cFont::GetFont(fontOsd);

      tArea control = { 1, 1, 600, 80, 4 };

      if ((status = osd->CanHandleAreas(&control, 1)) != oeOk)
      {
         tell(0, "Can't open osd, CanHandleAreas() returned (%d)", status);
         return ;
      }

      osd->SetAreas(&control, 1);
      cBitmap* _region = new cBitmap(control.Width(), control.Height(), control.bpp, control.x1, control.y1);

      _region->DrawText(5, 10, "graphTFT - Picture beside Picture", clrWhite, clrGray50, font);
      
      osd->DrawRectangle(0, 50, 150, 80, clrRed);
      osd->DrawText(10, 50, "Restart", clrWhite, clrRed, font);
      
      osd->DrawRectangle(150, 50, 300, 80, clrGreen);
      osd->DrawText(160, 50, "Switch", clrBlack, clrGreen, font);
      
      osd->DrawRectangle(300, 50, 450, 80, clrYellow);
      osd->DrawText(310, 50, "Back to...", clrBlack, clrYellow, font);
      
      osd->DrawRectangle(450,50,600,80, clrBlue);
      osd->DrawText(460, 50, "End PbP", clrWhite, clrBlue, font);
      
      osd->DrawBitmap(_region->X0(), _region->Y0(), *_region);
      osd->Flush();
   }
}

//***************************************************************************
// Process Key
//***************************************************************************

eOSState cGraphTFTOsdObject::ProcessKey(eKeys Key)
{
   eOSState state = cOsdObject::ProcessKey(Key);

   if (state == osUnknown) 
   {
      switch (Key & ~k_Repeat) 
      {
         case kUp:     break;
         case kDown:   break;
         case kRed:    break;
         case kGreen:  break;
         case kYellow: return osEnd;
         case kBlue:   return osEnd;
         case kBack:   return osEnd;
         case kOk:     return osEnd;

         default:      return state;
      }

      state = osContinue;
   }

   return state;
}
