/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  i18n.c - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: i18n.c,v 1.5 2007/11/10 17:45:09 root Exp $
 *
 **/

#if APIVERSNUM < 10507

#include "i18n.h"

const tI18nPhrase Phrases[] = {
  { "GraphTFT",
    "GraphTFT",
    "",// TODO
    "GraphTFT",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "GraphTFT",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Graph-TFT",//noch ben�tigt?
    "Graph-TFT",
    "",// TODO
    "Graph-TFT",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Graph-TFT",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Reload themes",
    "Themes neu laden",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Normal Display",
    "Standardanzeige",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Refresh Display",
    "Anzeige aktualisieren",
    "",// TODO
    "Aggiorna",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Picture beside Picture",
    "Bild neben Bild",
    "",// TODO
    "Immagine dietro immagine",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Kuva kuvan viereen",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Hide Mainmenu Entry",
    "Hauptmenueintrag verstecken",
    "",// TODO
    "Nascondi voce nel menu principale",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Piilota valinta p��valikosta",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Theme",
    "Aussehen",
    "",// TODO
    "Tema",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Ulkoasu",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Unknown title",
    "Unbekannter Titel",// TODO
    "",// TODO
    "Titolo sconosciuto",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Debug Level",
    "Debug Level",// TODO-DE
    "",// TODO
    "Livello degub",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Debug-taso",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Log Device",
    "Log Device",// TODO-DE
    "",// TODO
    "Log scheda",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Debug-loki",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Border to Width",
    "Rahmen zur Hoehe",// TODO
    "",// TODO
    "Larghezza bordo",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Leveysraja",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Border to Height",
    "Rahmen zur Breite",// TODO
    "",// TODO
    "Altezza bordo",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Korkeusraja",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "X Offset",
    "X Koordinaten Abweichung",// TODO
    "",// TODO
    "Limite X",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Vaakakeskitys",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Y Offset",
    "Y Koordinaten Abweichung",// TODO
    "",// TODO
    "Limite Y",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Pystykeskitys",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Convert Iso to UTF-8",
    "Konvertiere ISO zu UTF-8",// TODO
    "",// TODO
    "Converti ISO in UTF-8",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Muunna ISO UTF-8:ksi",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Dump image height",
    "Dump Bild Hoehe",// TODO
    "",// TODO
    "Estrai altezza immagine",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Tallennettavan kuvan korkeus",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Dump image width",
    "Dump Bild Breite",// TODO
    "",// TODO
    "Estrai larghezza immagine",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Tallennettavan kuvan leveys",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Dump image to file",
    "Dump Bild in Datei speichern",// TODO
    "",// TODO
    "Estrai immagine in un file",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Tallenna kuva tiedostoon",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Use StillPicture",
    "Benutze StillPicture",// TODO
    "",// TODO
    "Utilizza Adatta Immagine",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "K�yt� StillPicture-toimintoa",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Refresh",
    "Aktualisieren",// TODO
    "",// TODO
    "Aggiorna",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "P�ivit�",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "DVB Device",
    "",// TODO
    "",// TODO
    "Scheda DVB",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "DVB-laite",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "DVB/FB Device",
    "",// TODO
    "",// TODO
    "Scheda DVB/FB",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "DVB/FB-laite",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "directFB Device",//noch genutzt?
    "",// TODO
    "",// TODO
    "Scheda directFB",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "DirectFB-laite",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Debug",
    "",// TODO
    "",// TODO
    "Debug",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "Debug",
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Can't save snapshot, missing event information",
    "Kann Snapshot nicht speichern, es fehlen Event Informationen",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Snapshot saved",
    "Snapshot gespeichert",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Error saving snapshot",
    "Fehler beim Snapshot speichern",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Snapshot",
    "",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Snapshot width",
    "Snapshot Breite",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Snapshot height",
    "Snapshot Hoehe",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Snapshot Jpeg Quality",
    "Snapshot JPEG Qualitaet",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Snapshot path",
    "Snapshot Ablagepfad",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Force redraw every [sec]",
    "Erzwungenes Neuzeichnen alle [Sek]",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Spectrum Analyzer",
    "",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Dump Image",
    "Dump Bild",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Dump Refresh",
    "Dump alle [Sek] erneuern",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Flip OSD",
    "Tausche OSD",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Width",
    "Breite",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Height",
    "Hoehe",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "TCP Connection",
    "TCP Verbindung",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Jpeg Quality",
    "JPEG Qualitaet",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "touch Device",
    "",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Device",
    "",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Test",
    "",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Calibrate",
    "Kalibrieren",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Stop",
    "",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "--- end of playlist ---",
    "--- Ende der Abspielliste ---",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "No EPG data available.",
    "Keine EPG Daten vorhanden.",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Title",
    "Titel",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "Artist",
    "Interpret",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { "graphTFT: The syntax version of the theme file don't match!",
    "graphTFT: Die Version des Themes passt nicht zur graphTFT Version!",// TODO-DE
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
    "",// TODO
#if VDRVERSNUM >= 10302
    "",// TODO
#endif
#if VDRVERSNUM >= 10307
    "",// TODO
#endif
  },
  { NULL }
  };

//***************************************************************************
#endif // APIVERSNUM < 10507