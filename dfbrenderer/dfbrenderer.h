/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  dfbrenderer.h - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: dfbrenderer.h,v 1.11 2007/12/03 19:58:30 root Exp $
 *
 **/

//The most part of this Code is from the softmpeg Plugin (part of the libsoftmpeg Project) witch is under LGPL:

#ifndef __GTFT_DFBRENDERER_H
#define __GTFT_DFBRENDERER_H

#include <directfb.h>

#ifdef HAVE_SOFTMPEG
#  include "softmpeg.h"
#else
#  ifdef HAVE_FFMPEG
#    include "mpeg2decoder.h"
#  endif
#endif

#include "common.h"
#include "renderer.h"
#include <map>

using std::map;

#define DFBCHECK(x...)                                              \
   do                                                               \
   {                                                                \
      DFBResult err = x;                                            \
      if (err != DFB_OK)                                             \
      {                                                              \
         tell(0, "dfbdevout.c: %s <%d>:\n\t", __FILE__, __LINE__ );  \
         status = fail;                                              \
         goto Catch;                                                 \
      }                                                              \
   }                                                                 \
   while (0)

#define COLORKEY 17,8,79

//***************************************************************************
// DFbRenderer
//***************************************************************************

class DFbRenderer : public Renderer
{
   public:

      enum Misc
      {
         maxFontPaths = 20
      };

      DFbRenderer(int x, int y, int width, int height, 
                  string cfgPath, int utf, string thmPath);
      ~DFbRenderer();
  
      int init(int devnum);
      void deinit();

      int dfb_init(int width, int height);
      int dfb_destroy();

      void setFontPath(string fntPath);
	
      void refresh(int force = no);
      void clear();

      int playVideo(const uchar *Data, int Length);
      bool devicePoll(cPoller &Poller, int TimeoutMs = 0);
      void deviceClear();
      void setPlayMode(bool Video);

      void dumpImage2File(const char* fname, int dumpWidth, int dumpHeight, const char* aPath = 0);

      int charWidthOf(const char* fontName = 0, int fontSize = 0);
      void image(const char* fname, int x, int y, 
                 int coverwidth, int coverheight, 
                 bool fit = false, bool aspectRatio = false);
      void imagePart(const char* fname, int x, int y,
                     int width, int height);
      int text(const char *text, const char *font_name, 
               int size, int align, int x, int y, int r, int g, int b, 
               int width, int height, int lines, 
               int dots = 0, int skipLines = 0);

      int lineCount(const char* text, const char* font_name, 
                    int size, int width);

      void rectangle(int x, int y, int width, int height, int r, int g, int b, int a);

   protected:

      void transform(int &x, int &y);
      void transform(int &x, int &y, 
                     int &w, int &h);

      string toFullFontPath(const char* font);

#ifndef HAVE_SOFTMPEG
# ifdef HAVE_FFMPEG
      cMpeg2Decoder* _decoder;
# endif
#endif

      int _initialized;
      bool _showImage;
      string fontPaths[maxFontPaths];
};

#endif //__GTFT_DFBRENDERER_H
