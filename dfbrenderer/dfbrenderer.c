/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  dfbrenderer.c - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: dfbrenderer.c,v 1.9 2007/12/03 19:58:30 root Exp $
 *
 **/

//The most part of this Code is from the softmpeg Plugin (part of the libsoftmpeg Project) witch is under LGPL:

#include <stdlib.h>

#include "directfb.h"
#include "dfbrenderer.h"
#include "cache.h"
#include "common.h"

static IDirectFB* dfb = 0;
static IDirectFBDisplayLayer* primarylayer = 0;
static IDirectFBWindow* window = 0;
static DFBWindowID windowid = (DFBWindowID)-1;
static IDirectFBSurface* info_surface = 0;
static DFBDisplayLayerID layerid = (DFBDisplayLayerID)-1;

#ifdef HAVE_SOFTMPEG
  struct softmpeg_decoder* sdec = 0;
#endif

int videolayer_is_overlay;

//***************************************************************************
// 
//***************************************************************************

DFBEnumerationResult enum_layers_callback(unsigned int id, DFBDisplayLayerDescription desc, void *data)
{
   // We take the first video layer not being the primary

   if (id != DLID_PRIMARY && desc.caps & DLCAPS_SURFACE && desc.type & DLTF_VIDEO)
   {
      layerid = id;
      return DFENUM_CANCEL;
   }

   return DFENUM_OK;
}

//***************************************************************************
// 
//***************************************************************************

DFbRenderer::DFbRenderer(int x, int y, int width, int height, string cfgPath, 
                         int utf, string thmPath)
   : Renderer(x, y, width, height, cfgPath, utf, thmPath)
{
   _initialized = no;
}

DFbRenderer::~DFbRenderer()
{
   deinit();
}

//***************************************************************************
// 
//***************************************************************************

int DFbRenderer::dfb_init(int width, int height)
{
   int status = success;

   DFBCHECK(DirectFBInit(0, 0));
   DFBCHECK(DirectFBSetOption("no-cursor", 0));
   DFBCHECK(DirectFBCreate(&dfb));

   DFBCHECK(dfb->GetDisplayLayer(dfb, DLID_PRIMARY, &primarylayer));
   DFBCHECK(primarylayer->SetCooperativeLevel(primarylayer, DLSCL_ADMINISTRATIVE));
   DFBCHECK(dfb->EnumDisplayLayers(dfb, enum_layers_callback, 0));

   if (videolayer_is_overlay)
   {
      DFBCHECK(primarylayer->SetBackgroundColor(primarylayer, COLORKEY, 0xff));
      layerid = (DFBDisplayLayerID)-1;
   }

   if (layerid == (DFBDisplayLayerID)-1)
   {
      DFBWindowDescription windowdesc;

      tell(1, "dfbdevout.c: could not find suitable videolayer - using window on primary layer");

      windowdesc.flags = (DFBWindowDescriptionFlags)(DWDESC_HEIGHT | DWDESC_WIDTH);
      windowdesc.width = 720;
      windowdesc.height = 576;

      DFBCHECK(primarylayer->CreateWindow(primarylayer, &windowdesc, &window));
      window->GetID(window, &windowid);
      window->SetOpacity(window, 0xff);
   }
   else
   {
      tell(1, "dfbdevout.c: using videolayer");
   }

   DFBSurfaceDescription dsc;
   dsc.flags = (DFBSurfaceDescriptionFlags)(DSDESC_CAPS | DSDESC_WIDTH | DSDESC_HEIGHT);
   dsc.caps = (DFBSurfaceCapabilities)(DSCAPS_PRIMARY | DSCAPS_FLIPPING);
   dsc.width = width;
   dsc.height = height;
   DFBCHECK(dfb->CreateSurface(dfb, &dsc, &info_surface));

  Catch:

   return status;
}

//***************************************************************************
// 
//***************************************************************************

int DFbRenderer::dfb_destroy()
{
   // delete images and fonts

   fontCache.Clear();

   if (info_surface)
      info_surface->Release(info_surface);

   if (window)
      window->Release(window);

   if (primarylayer)
      primarylayer->Release(primarylayer);

   if (dfb)
      dfb->Release(dfb);

   return done;
}

//***************************************************************************
// 
//***************************************************************************

void DFbRenderer::deinit()
{
   if (!_initialized)
      return;

#ifdef HAVE_SOFTMPEG
   if (sdec)
      softmpeg_decoder_destroy(sdec);
#endif

   dfb_destroy();

   _initialized = no;
}

//***************************************************************************
// Init
//***************************************************************************

int DFbRenderer::init(int devnum)
{
   int status = success;

#ifdef HAVE_SOFTMPEG
   struct softmpeg_directfb_initdata init;
#endif

   if (getenv("DIRECTFB_VIDEO_IS_OVERLAY"))
      videolayer_is_overlay = 1;
   else
      videolayer_is_overlay = 0;

   if (dfb_init(dspWidth, dspHeight) != success)
   {
      tell(0, "Initializing of directFB failed!");
      return fail;
   }

#ifdef HAVE_SOFTMPEG
   init.window_id = windowid;
   init.layer_id = layerid;
   init.width = dspWidth;
   init.height = dspHeight;

   sdec = softmpeg_decoder_create(0, 0, SOFTMPEG_DIRECTFB, &init, SOFTMPEG_FUSIONSOUND, 0);
#endif

   if (layerid != (DFBDisplayLayerID)-1 && videolayer_is_overlay)
   {
      IDirectFBDisplayLayer *videolayer;
      DFBDisplayLayerDescription desc;
      DFBDisplayLayerConfig dlc;
      IDirectFBSurface *surface;

      DFBCHECK(dfb->GetDisplayLayer(dfb, layerid, &videolayer));
      DFBCHECK(videolayer->SetCooperativeLevel(videolayer, DLSCL_ADMINISTRATIVE));

      // make sure to use destination colorkeying, so that our OSD is visible

      DFBCHECK(videolayer->GetDescription(videolayer, &desc));
      dlc.flags = (DFBDisplayLayerConfigFlags) DLCONF_OPTIONS;
      dlc.options = (DFBDisplayLayerOptions) DLOP_DST_COLORKEY;
      DFBCHECK(videolayer->SetConfiguration(videolayer, &dlc));
      DFBCHECK(videolayer->SetDstColorKey(videolayer, COLORKEY));

      DFBCHECK(videolayer->GetSurface(videolayer, &surface));
      DFBCHECK(surface->Clear(surface, COLORKEY, 0xff));
      DFBCHECK(surface->Flip(surface, 0, (DFBSurfaceFlipFlags)DSFLIP_ONSYNC));
   }

  Catch:

   if (status == success)
      _initialized = yes;

   return status;
}

//***************************************************************************
// Set Font Path
//***************************************************************************

void DFbRenderer::setFontPath(string fntPath)
{
   int e, s;
   e = s = 0;
   string p;
   char* path;
   int count = 0;

   // first clear list

   for (int i = 0; i < maxFontPaths; i++)
      fontPaths[i] = "";

   // add all configured font paths

   tell(1, "Info: font path configured to '%s'", fntPath.c_str());
   
   do
   {
      e = fntPath.find(':', s);
      p = fntPath.substr(s, e == na ? fntPath.length()-s : e-s);

      if (p[0] != '/')
      {
         // make path relative to the themes directory

         asprintf(&path, "%s/graphTFT/themes/%s/%s", 
                  confPath.c_str(), themePath.c_str(), p.c_str());
      }
      else
      {
         asprintf(&path, "%s", p.c_str());
      }

      tell(0, "Info: Adding font path '%s'", path);

      if (*path && fileExists(path))
         fontPaths[count] = path;
      else
         tell(0, "Info: Font path '%s' not found, ignoring", path);

      free(path);
      s = e+1;
      count++;

   } while (s > 0 && count < maxFontPaths-1);
   
   
   // at least add the default path

   asprintf(&path, "%s/graphTFT/fonts/", confPath.c_str());
   tell(0, "Info: Adding font path '%s'", path);
   fontPaths[count] = path;
   free(path);
}

//***************************************************************************
// 
//***************************************************************************

string DFbRenderer::toFullFontPath(const char* font)
{
   string path = "";

   for (int i = 0; i < maxFontPaths; i++)
   {
      if (fontPaths[i] == "")
         continue;

      path = fontPaths[i] + "/" + font + ".ttf";

      if (fileExists(path.c_str()))
         break;
   }

   return path;
}

//***************************************************************************
// 
//***************************************************************************

void DFbRenderer::transform(int& x, int& y)
{
   tell(4, "dfbdevout.c: transform(2)");

   x = x * dspWidth/themeWidth;
   y = y * dspHeight/themeHeight;
}

//***************************************************************************
// 
//***************************************************************************

void DFbRenderer::transform(int& x, int& y, int& w, int& h)
{
   tell(4, "dfbdevout.c: transform(4)");

   x = x * dspWidth / themeWidth;
   y = y * dspHeight / themeHeight;
   w = w * dspWidth / themeWidth;
   h = h * dspHeight / themeHeight;
}

//***************************************************************************
// 
//***************************************************************************

void DFbRenderer::refresh(int force)
{
   int status = success;

   tell(4 , "dfbdevout.c: refresh()");
   DFBCHECK(info_surface->Flip(info_surface, 0, (DFBSurfaceFlipFlags)DSFLIP_ONSYNC));
   tell(5 , "dfbdevout.c: leave refresh()");

  Catch:
   ;
}

void DFbRenderer::clear()
{
   int status = success;
   tell(4 , "dfbdevout.c: clear()");
   DFBCHECK(info_surface->Clear(info_surface, 0, 0, 0, 0));
   tell(5 , "dfbdevout.c: leave clear()");

  Catch:
   ;
}

//***************************************************************************
// 
//***************************************************************************

void DFbRenderer::setPlayMode(bool Video)
{
   tell(4 , "dfbdevout.c: SetPlayMode()");
   _showImage = !Video;

   if (Video)
   {
      ;//info_surface->SetOpacity(info_surface, 0xff);
#ifdef HAVE_SOFTMPEG
#else
# ifdef HAVE_IMLIB
      _showImage = no;
      //_decoder = new cMpeg2Decoder(frame_buffer,PIX_FMT_YUV, dspWidth, dspHeight);
      //_decoder->Start();
# endif
#endif

   }
   else
   {
      ;//info_surface->SetOpacity(info_surface, 0x00);
#ifdef HAVE_SOFTMPEG
      if (sdec) softmpeg_decoder_stop(sdec);
#else
# ifdef HAVE_IMLIB
      //if (_decoder){
      // _decoder->Stop();
      // delete(_decoder);
      //}
# endif
#endif
   }
}

//***************************************************************************
// 
//***************************************************************************

int DFbRenderer::playVideo(const uchar *Data, int Length)
{
   tell(7 , "dfbdevout.c: playVideo()");
#ifdef HAVE_SOFTMPEG
   if (sdec)
   {
      int delay = 0;
      tell(8 , "dfbdevout.c: PlayVideo: %d bytes",Length);
      delay = softmpeg_decoder_process_pes_data(sdec,(unsigned char*)Data,Length);
      if (0 != delay)
      {
         struct timespec req;
         req.tv_sec = delay / 1000;
         req.tv_nsec = (delay % 1000) * 1000 * 1000;
         tell(4 , "dfbdevout.c: PlayVideo delaying %d ms",delay);
         nanosleep(&req, 0);
      }
      return Length;
   }
#else
#ifdef HAVE_IMLIB
   /*
   int result=_decoder->Decode(Data, Length);
   // restart the decoder
   if (result == -1) {
    delete(_decoder);
    _decoder= new cMpeg2Decoder(frame_buffer,PIX_FMT_YUV, dspWidth, dspHeight);
    return 0;
}
   return Length;
   */
#endif
#endif
   return 0;
}

//***************************************************************************
// 
//***************************************************************************

bool DFbRenderer::devicePoll(cPoller &Poller, int TimeoutMs)
{
   tell(7 , "dfbdevout.c: devicePoll()");

   // For now lets say we always ready

   return true;
}

void DFbRenderer::deviceClear(void)
{
   tell(4 , "dfbdevout.c: deviceClear()");

   // Fill Framebuffer with zero

   clear();
}

//***************************************************************************
// 
//***************************************************************************

void DFbRenderer::image(const char* fname, int x, int y,
                        int width, int height, 
                        bool fit, bool aspectRatio)
{
   int status = success;
   char* fpath;
   IDirectFBImageProvider* image;
   IDirectFBSurface* render_surface = 0;
   
   if (!fname)
      return;

   if (fname[0] == '/')
      asprintf(&fpath, "%s", fname);
   else
   {
      const char* tmp = themePath.c_str();
      asprintf(&fpath, "%s/graphTFT/themes/%s/%s", confPath.c_str(), tmp, fname);
   }

   tell(4,"creating imageprovider for %s", fpath);

   if (dfb->CreateImageProvider(dfb, fpath, &image) == 0)
   {
      DFBSurfaceDescription image_desc;
      DFBRectangle rect;
      DFBCHECK(image->GetSurfaceDescription(image, &image_desc));

      if (fit)
      {
         if (aspectRatio)
         {
            int areaWidth = width;
            int areaHeight = height;
   
            double ratio = (double)image_desc.width / (double)image_desc.height;
               
            if ((double)width/(double)image_desc.width < (double)height/(double)image_desc.height)
            {
               height = (int)((double)width / ratio);
               y += (areaHeight-height) / 2;
            }
            else
            {
               width = (int)((double)height * ratio);
               x += (areaWidth-width) / 2;
            }
         }

         image_desc.width = width; 
         image_desc.height = height;
      }

      transform(x, y, image_desc.width, image_desc.height);
      DFBCHECK(dfb->CreateSurface(dfb, &image_desc, &render_surface));
      rect.x = 0; rect.y = 0;
      rect.w = image_desc.width;
      rect.h = image_desc.height;
      DFBCHECK(image->RenderTo(image, render_surface, &rect));
      image->Release(image);
   }

   free(fpath);

   if (render_surface)
   {
      DFBCHECK(info_surface->SetBlittingFlags(info_surface, DSBLIT_BLEND_ALPHACHANNEL));
      DFBCHECK(info_surface->Blit(info_surface, render_surface, 0, x, y));
   }

  Catch:
   ;
}

//***************************************************************************
// Image Part
//***************************************************************************

void DFbRenderer::imagePart(const char* fname, int x, int y,
                            int width, int height)
{
   int status = success;
   char* fpath;
   IDirectFBImageProvider* image;
   IDirectFBSurface* render_surface = 0;
   DFBRectangle rect;
 
   if (!fname)
      return;

   if (fname[0] == '/')
      asprintf(&fpath, "%s", fname);
   else
   {
      const char* tmp = themePath.c_str();
      asprintf(&fpath, "%s/graphTFT/themes/%s/%s", confPath.c_str(), tmp, fname);
   }

   tell(4,"creating imageprovider for %s", fpath);

   if (dfb->CreateImageProvider(dfb, fpath, &image) == 0)
   {
      DFBSurfaceDescription image_desc;
      DFBCHECK(image->GetSurfaceDescription(image, &image_desc));

      transform(x, y, image_desc.width, image_desc.height);
      DFBCHECK(dfb->CreateSurface(dfb, &image_desc, &render_surface));

      rect.x = 0; rect.y = 0;
      rect.w = image_desc.width;
      rect.h = image_desc.height;

      // render image to rect of render_surface (scale to rect)

      DFBCHECK(image->RenderTo(image, render_surface, &rect));
      image->Release(image);
   }

   free(fpath);

   if (render_surface)
   {
      rect.x = x; 
      rect.y = y;
      rect.w = width;
      rect.h = height;

      DFBCHECK(info_surface->SetBlittingFlags(info_surface, DSBLIT_BLEND_ALPHACHANNEL));
      DFBCHECK(info_surface->Blit(info_surface, render_surface, &rect, x, y));
   }
  Catch:
   ;
}

//***************************************************************************
// Char Width Of
//***************************************************************************

int DFbRenderer::charWidthOf(const char* fontName, int fontSize)
{ 
   int status = success;
   IDirectFBFont* font = 0;
   int width = 20;

   const char* text = "We need here something like a "
      "representive sentence WITH SOME UPPER CASE LETTERS";

   if (!fontCache.Contains(FontFaceSize(fontName, fontSize)))
   {
      DFBFontDescription desc;
      string fontPath = toFullFontPath(fontName);

      if (!fileExists(fontPath.c_str()))
      {
         tell(0, "dfbdevout.c: Font '%s' not found!", fontPath.c_str());
      }
      else
      {
         tell(4, "creating font for %s", fontPath.c_str());
         
         desc.flags = DFDESC_HEIGHT;
         desc.height = fontSize * 4/3;
         transform(desc.width, desc.height);
         
         if (dfb->CreateFont(dfb, fontPath.c_str(), &desc, &font) == 0)
            fontCache[FontFaceSize(fontName, fontSize)] = font;
      }
   }
   else
   {
      font = fontCache[FontFaceSize(fontName, fontSize)];
   }

   if (font)
   {
      int textWidth;

      DFBCHECK(info_surface->SetFont(info_surface, font));
      DFBCHECK(font->GetStringWidth(font, text, -1, &textWidth));

      width = textWidth / strlen(text);
   }

  Catch:

   return width;
}

//***************************************************************************
// Line Count
//***************************************************************************

int DFbRenderer::lineCount(const char* text, const char* font_name, 
                           int size, int width)
{
   // #TODO !!

   return 20;
}

//***************************************************************************
// Text
//***************************************************************************

int DFbRenderer::text(const char* text, 
                      const char* font_name, int size, int align, 
                      int x, int y,
                      int r, int g, int b, 
                      int width, int height, int lines, 
                      int dots, int skipLines)
{
   int status = success;
   IDirectFBFont* font;
   DFBFontDescription desc;
   string fontPath;
   int lineCount = 0;

   if (Str::isEmpty(text))
      return 0;
   
   if (utf8)
   {
      const int maxBuf = 10000;
      char out[maxBuf+TB]; *out = 0;
       
      if (toUTF8(out, maxBuf, text) == success)
         text = out;
   }

   if (!fontCache.Contains(FontFaceSize(font_name, size)))
   {
      fontPath = toFullFontPath(font_name);

      if (!fileExists(fontPath.c_str()))
      {
         tell(0, "dfbdevout.c: Font '%s' not found!", fontPath.c_str());
         return 0;
      }

      tell(4, "creating font for %s", fontPath.c_str());

      desc.flags = DFDESC_HEIGHT;
      desc.height = size*4/3;
      transform(desc.width, desc.height);

      if (dfb->CreateFont(dfb, fontPath.c_str(), &desc, &font) == 0)
         fontCache[FontFaceSize(font_name, size)] = font;
   }
   else
      font = fontCache[FontFaceSize(font_name, size)];

   if (font)
   {
      int lineHeight, textWidth, pos, index, maxLines;
      string tmp = text, tmp2;
      DFBRegion rgn;

      transform(x, y, width, height);
      DFBCHECK(info_surface->SetColor(info_surface, r, g, b, 0xff));
      rgn.x1 = x; rgn.y1 = y; rgn.x2 = x+width-1, rgn.y2 = y+height-1;
      DFBCHECK(info_surface->SetClip(info_surface, &rgn));
      DFBCHECK(info_surface->SetFont(info_surface, font));
      DFBCHECK(font->GetHeight(font, &lineHeight));
      DFBCHECK(font->GetStringWidth(font, text, -1, &textWidth));

      if (lines != 0)
         maxLines = lines;
      else
         maxLines = max(height/lineHeight, 1);

      lineCount = 1;

      while (textWidth > width && textWidth > 0 && lineCount <= maxLines)
      {
         int w3 = 0;

         for (index = 1; w3 < width; ++index)
         {
            string tmp3 = tmp.substr(0, index);
            DFBCHECK(font->GetStringWidth(font, tmp3.c_str(), -1, &w3));
         }

         tmp2 = tmp.substr(0, index+1);
         pos = tmp2.rfind(' ');

         if (pos > 0)
         {
            tmp2.resize(pos);
            tmp = tmp.substr(pos+1);
         }
         else
         {
            tmp2.resize(index-2);
            tmp = tmp.substr(index-2);
         }

         DFBCHECK(font->GetStringWidth(font, tmp2.c_str(), -1, &textWidth));

         if (align == 0)
            DFBCHECK(info_surface->DrawString(info_surface, tmp2.c_str(), 
                                              -1, x, y + (lineHeight * (lineCount-1)), DSTF_TOP));
         else if (align == 1)
            DFBCHECK(info_surface->DrawString(info_surface, tmp2.c_str(), 
                                              -1, x + (width - textWidth) / 2, y + (lineHeight * (lineCount-1)), DSTF_TOP));
         else
            DFBCHECK(info_surface->DrawString(info_surface, tmp2.c_str(), 
                                              -1, x + width - textWidth, y + (lineHeight * (lineCount-1)), DSTF_TOP));

         lineCount++;
         DFBCHECK(font->GetStringWidth(font, tmp.c_str(), -1, &textWidth));
      }

      if (lineCount <= maxLines && tmp.size() > 0)
      {
         if (align == 0)
            DFBCHECK(info_surface->DrawString(info_surface, tmp.c_str(), 
                                              -1, x, y + (lineHeight * (lineCount-1)), DSTF_TOP));
         else if (align == 1)
            DFBCHECK(info_surface->DrawString(info_surface, tmp.c_str(), 
                                              -1, x + (width - textWidth) / 2, y + (lineHeight * (lineCount-1)), DSTF_TOP));
         else
            DFBCHECK(info_surface->DrawString(info_surface, tmp.c_str(), 
                                              -1, x + width - textWidth, y + (lineHeight * (lineCount-1)), DSTF_TOP));
      }
      else
         lineCount--;

      DFBCHECK(info_surface->SetFont(info_surface, 0));
      DFBCHECK(info_surface->SetClip(info_surface, 0));
   }

  Catch:

   return lineCount;
}

//***************************************************************************
// 
//***************************************************************************

void DFbRenderer::rectangle(int x, int y, 
                            int width, int height, 
                            int r, int g, int b, 
                            int a)
{
   int status = success;

   transform(x, y, width, height);

   if (width <= 0 || height <= 0)
      return;

   DFBCHECK(info_surface->SetColor(info_surface, r, g, b, a));
   DFBCHECK(info_surface->FillRectangle(info_surface, 
                                        x, y, width, height));

  Catch:
   ;
}

//***************************************************************************
// 
//***************************************************************************

void DFbRenderer::dumpImage2File(const char* fname, int dumpWidth, 
                                 int dumpHeight, const char* aPath)
{
   std::string fileName;

   if (!fname || *fname == 0)
      fileName = "graphTFT";
   else
      fileName = fname;

   info_surface->Dump(info_surface, aPath ? aPath : "/tmp", fileName.c_str());

   tell(4, "dfbrenderer.c: image dumped to %s/%s", aPath ? aPath : "/tmp", fileName.c_str());
}
