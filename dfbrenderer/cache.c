/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  cache.c - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: cache.c,v 1.1.1.1 2006/10/24 11:31:07 root Exp $
 *
 **/
#include "cache.h"

// #define IMAGECACHESIZE 10
#define FONTCACHESIZE   5

// ImageCache imageCache(IMAGECACHESIZE);
FontCache fontCache(FONTCACHESIZE);
