/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  osd.h - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: osd.h,v 1.2 2007/05/31 16:52:08 root Exp $
 *
 **/

#ifndef __GTFT_OSD_H
#define __GTFT_OSD_H

#include "display.h"
#include <vdr/plugin.h>

//***************************************************************************
// Class cGraphTFTOsdObject
//***************************************************************************

class cGraphTFTOsdObject : public cOsdObject
{
   public:

      cGraphTFTOsdObject(cGraphTFTDisplay* _display);
      virtual ~cGraphTFTOsdObject();

      void Show();
      eOSState ProcessKey(eKeys Key);

   private:

      cGraphTFTDisplay* _display;
      cOsd* osd;
};

//***************************************************************************
#endif // __GTFT_OSD_H
