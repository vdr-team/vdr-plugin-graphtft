/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  memcpy.h - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: memcpy.h,v 1.2 2006/11/06 10:29:23 root Exp $
 *
 **/

//The most part of this Code is from the (great and my favorit) analogtv plugin:

#ifndef __GTFT_MEMCPY_H_
#define __GTFT_MEMCPY_H_

/* optimized/fast memcpy */

extern void *(*fast_memcpy)(void* to, const void* from, size_t len);

/* benchmark available memcpy methods */

int probe_fast_memcpy(int method, bool force = false);

enum 
{
  MEMCPY_PROBE = 0,
  MEMCPY_GLIBC,
  MEMCPY_KERNEL,
  MEMCPY_MMX,
  MEMCPY_MMXEXT,
  MEMCPY_SSE
};

#endif // __GTFT_MEMCPY_H_
