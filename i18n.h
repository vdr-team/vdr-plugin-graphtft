/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  i18n.h - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: i18n.h,v 1.2 2007/12/03 19:58:20 root Exp $
 *
 **/

#ifndef __GTFT_I18N_H
#define __GTFT_I18N_H

#include <vdr/i18n.h>

#if APIVERSNUM < 10507
#  define trVDR(s)  I18nTranslate(s)
   extern const tI18nPhrase Phrases[];
#endif

#endif //__GTFT_I18N_H
