/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  transfer.h - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: transfer.h,v 1.1 2007/05/31 16:52:08 root Exp $
 *
 **/

#ifndef __GTFT_TRANSFER_H
#define __GTFT_TRANSFER_H

//***************************************************************************
// Includes
//***************************************************************************

#include "vdr/player.h"
#include "vdr/receiver.h"
#include "vdr/remux.h"
#include "vdr/ringbuffer.h"
#include "vdr/thread.h"
#include "vdr/channels.h"

#include <renderer.h>

//***************************************************************************
// Defenitions
//***************************************************************************

#define VIDEOBUFSIZE  MEGABYTE(4)
#define POLLTIMEOUTS_BEFORE_DEVICECLEAR (1000 /* ms */ * 31 /* 2 GOPs + 1 additional frame */ / 25 /* frames per second */ / 100 /* ms poll timeout */)

//***************************************************************************
// Class cGraphTFTTransfer
//***************************************************************************

class cGraphTFTTransfer : public cReceiver, public cPlayer, public cThread 
{
   public:

      cGraphTFTTransfer(Renderer* renderer, const cChannel* Channel);
      virtual ~cGraphTFTTransfer();

      virtual int NumAudioTracks() const;
      virtual const char** GetAudioTracks(int *CurrentTrack = NULL) const;
      // virtual void SetAudioTrack(eTrackType Type, const tTrackId* TrackId);

   protected:

      virtual void Activate(bool On);
      virtual void Receive(uchar *Data, int Length);
      virtual void Action();

   private:

      cRingBufferLinear* _ringBuffer;
      cRemux* _remux;
      bool _canToggleAudioTrack;
      uchar _audioTrack;
      Renderer *_render;
      bool _gotBufferReserve;
      bool _active;
      void StripAudioPackets(uchar* b, int Length, uchar Except = 0x00);

};

//***************************************************************************
#endif //__GTFT_TRANSFER_H
