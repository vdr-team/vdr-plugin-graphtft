

#include <sysinfo.h>
#include <unistd.h>

/*

  g++ cpuload-test.c  sysinfo.c  -I. `pkg-config libgtop-2.0 --cflags` `pkg-config libgtop-2.0 --libs` -DWITH_SYSINFO -DWITHOUT_VDR -o cpu

*/

int main()
{
  
   for (int i = 0; i < 1000; i++)
   {
      int load = Sysinfo::cpuLoad();

      printf("cpu%% %2d, idle%% %2d\n", load, 100 - load);
      
      // usleep(200000);
      sleep(1);
   }

   return done;
}
