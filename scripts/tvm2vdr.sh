#!/bin/bash

unset ${!LC_*} LANGUAGE
export LANG="de_DE.UTF-8"

cd /usr/lib/vdr/tvmovie2vdr-0.5.14

if [ -f /etc/vdr/tvm2vdr.run ]; then
  echo already one tvm2vdr running
  echo if there is no tvm2vdr running, remove
  echo /etc/vdr/tvm2vdr.run and retry.
  exit
fi

touch /etc/vdr/tvm2vdr.run
echo starting at `date`

# cleanup old images
find /video0/epgimages/ -type f  -mtime +30 -exec rm {} \;

# start tvmovie2vdr
./tvm2vdr >> ./tvm2vdr.log 2>&1

# create _0 links, remove old first, Pfad anpassen oder aus config/

IMG_PATH=`grep "our.*imagepath" config/config.pl | sed s/"^.*= *\""/""/ | sed s/"\".*$"/""/`

if [ -d "$IMG_PATH" ]; then
  cd "$IMG_PATH"
  echo "removing links"
  find . -type l -name "*[0-9]_0.png" -exec rm {} \;
  echo "creating links"
  find . -name "*[0-9][0-9][0-9][0-9].png" | while read f; do ln -s $f ./`basename $f .png`_0.png; done
else
   echo "cant create links, path $IMG_PATH not found"
fi

rm /etc/vdr/tvm2vdr.run
