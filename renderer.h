/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  renderer.h - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt
 *  (c) 2005-2008 J�rg Wendel
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: renderer.h,v 1.9 2007/12/03 19:58:20 root Exp $
 *
 **/

#ifndef __GTFT_RENDERER_H
#define __GTFT_RENDERER_H

#include <string>

#include <vdr/device.h>
#include <common.h>


using std::string;

//***************************************************************************
// used as base class of the renderes
//***************************************************************************

class Renderer
{
   public:

      Renderer(int x, int y, int width, int height, 
               string cfgPath, int utf, string thmPath);
      virtual ~Renderer();

      virtual void setProperties(int x, int y, 
                                 int width, int height, 
                                 int utf, string thmPath); 
      virtual void setBorder(int widthBorder, 
                              int heightBorder);
      virtual void setFontPath(string fntPath) = 0;
      virtual int init(int devnum) = 0;
      virtual void flushCache() {};
      virtual void deinit() = 0;

      virtual int getImageBuffer(const void*& buffer, 
                                 int quality)  { return 0; }
      virtual void refresh(int force = no) = 0;
      virtual void clear() = 0;

      // virtual int textWidthOf(const char* text, const char* fontName, int fontSize) = 0;
      virtual int charWidthOf(const char* fontName = 0, 
                              int fontSize = 0) = 0;
      virtual void image(const char* fname, int x, int y, 
                         int coverwidth, int coverheight, 
                         bool fit = false, bool aspectRatio = false) = 0;

      virtual void imagePart(const char* fname, int x, int y, 
                             int width, int height) = 0;

      virtual int text(const char *text, const char *font_name, 
                       int size, int align, int x, int y, 
                       int r, int g, int b, int width, int height,
                       int lines, int dots = 0, int skipLines = 0) = 0;

      virtual int lineCount(const char* text, const char* font_name, 
                            int size, int width) = 0;

      virtual void rectangle(int x, int y, int width, int height, 
                             int r, int g, int b, int a) = 0;

      virtual void dumpImage2File(const char* fname, 
                                  int dumpWidth, int dumpHeight, 
                                  const char* aPath = 0) = 0;

      virtual int playVideo(const uchar *Data, int Length) = 0;
      virtual bool devicePoll(cPoller &Poller, int TimeoutMs = 0) = 0;
      virtual void setPlayMode(bool Video) = 0;
      virtual void deviceClear() = 0;


   protected:

      string confPath;
      string themePath;
      int utf8;
      int xOffset;
      int yOffset;
      int themeWidth;
      int themeHeight;
      int xBorder;
      int yBorder;

      int dspWidth;
      int dspHeight;

};

//***************************************************************************
#endif // __GTFT_RENDERER_H
