//***************************************************************************
// Group VDR/GraphTFT
// File graphtft.h
// Date 31.10.06
// This code is distributed under the terms and conditions of the
// GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
// (c) 2006-2008 J�rg Wendel
//--------------------------------------------------------------------------
// Class cPluginGraphTFT
//***************************************************************************

#ifndef __GTFT__H__
#define __GTFT__H__

#include <vdr/plugin.h>
#include <vdr/config.h>

#if APIVERSNUM < 10507
#  include "i18n.h"
#  define trNOOP(s) (s)
#endif

#include "display.h"
#include "setup.h"

#include "common.h"

static const char* VERSION        = "0.3.3";
static const char* THEMEVERSION   = "0.3.1";
static const char* DESCRIPTION    = trNOOP("VDR OSD on TFT");

static const char* MAINMENUENTRY  = trNOOP("Graph-TFT");

void *(*fast_memcpy)(void* to, const void* from, size_t len);

class cPluginGraphTFT;

//***************************************************************************
// Class GraphTFTMenuItem
//***************************************************************************

class cGraphTFTMenuItem : public cOsdItem
{
   public:
   
      cGraphTFTMenuItem(const char* aTitle)
         : cOsdItem(aTitle) {}

};

//***************************************************************************
// Class GraphTFTMenu
//***************************************************************************

class cGraphTFTMenu : public cMenuSetupPage
{
   public:

      enum Size
      {
         sizeVarMax = 100
      };

      cGraphTFTMenu(const char* title, cPluginGraphTFT* aPlugin);
      virtual ~cGraphTFTMenu();
      
      virtual eOSState ProcessKey(eKeys key);

      const char* MenuKind() { return "MenuSetupPage"; }

   protected:

      void Store();

      cPluginGraphTFT* plugin;
      int defMode;
      int dspActive;
      char variableValues[50][sizeVarMax+TB];
};

//***************************************************************************
// Class cPluginGraphTFT
//***************************************************************************

class cPluginGraphTFT : public cPlugin, cThemeService
{
   public:

      cPluginGraphTFT();
      ~cPluginGraphTFT();

      const char* Version()          { return VERSION; }
      const char* Description()      { return tr(DESCRIPTION); }
      const char* CommandLineHelp();

      bool ProcessArgs(int argc, char* argv[]);
      bool Initialize();
      bool Start();
      void Store();
      void Housekeeping() {}

      const char* MainMenuEntry()    { return GraphTFTSetup.HideMainMenu ? 0 : tr(MAINMENUENTRY); }
      cOsdObject* MainMenuAction();

      cMenuSetupPage* SetupMenu();
      bool SetupParse(const char* Name, const char* Value);
      cGraphTFTDisplay* getDisplay() { return display; }

      bool Service(const char* Id, void* Data);
      const char** SVDRPHelpPages();
      cString SVDRPCommand(const char* Command, const char* Option, int& ReplyCode);

      int loadThemes();

   private:

      cGraphTFTDisplay* display;
      char* _dev;
      int port;
};

//***************************************************************************
#endif  // __GTFT__H__
