//***************************************************************************
// Group VDR/GraphTFT
// File dmyrenderer.c
// Date 31.10.06 - J�rg Wendel
// This code is distributed under the terms and conditions of the
// GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
//--------------------------------------------------------------------------
// Class DummyRenderer
//***************************************************************************

#ifndef __GTFT_DMYRENDERER_HPP__
#define __GTFT_DMYRENDERER_HPP__

#include "imlibrenderer.h"

class DummyRenderer : public ImlibRenderer
{
   public:	

      DummyRenderer(int x, int y, int width, int height, string cfgPath, int utf, string thmPath)
         : ImlibRenderer(x, y, width, height, cfgPath, utf, thmPath) { }

		int init(int devnum) { return ImlibRenderer::init(devnum); }
		void deinit()        { }

		int playVideo(const uchar *Data, int Length)          { return 0; }
		bool devicePoll(cPoller &Poller, int TimeoutMs = 0)   { return false; }
		void setPlayMode(bool Video)         { }
		void deviceClear(void)               { }
};

//***************************************************************************
#endif // __GTFT_DMYRENDERER_HPP__
