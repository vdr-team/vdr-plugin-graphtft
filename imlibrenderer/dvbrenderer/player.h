/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  player.c - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: player.h,v 1.2 2006/11/06 10:31:00 root Exp $
 *
 **/

#ifndef __GTFT_PLAYER_H
#define __GTFT_PLAYER_H

#include <vdr/thread.h>
#include <vdr/player.h>

#include "common.h"
#include "mpeg2encoder.h"

extern void* (*fast_memcpy)(void *to, const void *from, size_t len);

class cGraphTFTPlayer: public cPlayer, public cThread 
{
private:
	bool _active;
	bool _showImage;
	mpeg_frame _iframe;
	cMutex _mutex;
	cCondVar _doUpdate;
	
protected:
	virtual void Activate(bool On);
	virtual void Action(void);
	void send_pes_packet(unsigned char *data, int len, int timestamp);

public:
	cGraphTFTPlayer(void);
	virtual void setPlayMode(bool Video);
	virtual void setIFrame( mpeg_frame iframe );
	virtual ~cGraphTFTPlayer();
};

#endif // __GTFT_PLAYER_H
