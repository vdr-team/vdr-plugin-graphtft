/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  mpeg2encoder.h - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: mpeg2encoder.h,v 1.2 2007/11/10 17:46:54 root Exp $
 *
 **/

//The most part of this Code is from the MMS V.2 Project:

#ifndef __GTFT_MPEG2ENCODER_H
#define __GTFT_MPEG2ENCODER_H

typedef unsigned char UINT8;

//***************************************************************************
// 
//***************************************************************************

struct mpeg_frame 
{
   UINT8 *data; /* generated MPEG- frame */
   int size;    /* size of data in *data, data is malloced to a constant size, but only part is filled */
};

typedef struct mpeg_frame mpeg_frame;

/* set up a few globals and initialize codec */
void mpeg_init(int video_width, int video_height, int fps);

/* send a BGRA buffer to this with *buf, and its size and you will get an mpeg_frame in return */
mpeg_frame mpeg_draw(UINT8 *buf);

/* cleanup and free */
void mpeg_deinit();

#endif //__GTFT_MPEG2ENCODER_H
