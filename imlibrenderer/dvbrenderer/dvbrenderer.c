/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  dvbrenderer.c - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: dvbrenderer.c,v 1.7 2007/11/10 17:46:54 root Exp $
 *
 **/

#include "common.h"
#include "dvbrenderer.h"
#include "setup.h"
#include "player.h"
#include "quantize.h"
#include <vdr/config.h>

//----------------------------------------------------------------------

void DvbRenderer::deinit()
{
   if (_Osd)       delete (_Osd);
   if (_quantizer) delete (_quantizer);
   _initialized = na;
}

//----------------------------------------------------------------------

int DvbRenderer::init(int devnum)
{
	mpeg_init(dspWidth, dspHeight, 25);

	_devTo = cDevice::GetDevice(devnum);
	_player = new cGraphTFTPlayer();
	_devTo->AttachPlayer(_player);
	_initialized = 1;

   return ImlibRenderer::init(devnum);
}

//----------------------------------------------------------------------

DvbRenderer::DvbRenderer(int x, int y, int width, int height, 
                         string cfgPath, int utf, string thmPath)
   : ImlibRenderer(x, y, width, height, cfgPath, utf, thmPath)
{
   _Osd = 0;
   _quantizer = 0;
   _initialized = na;
}

DvbRenderer::~DvbRenderer()
{
   deinit();
}

//----------------------------------------------------------------------
void DvbRenderer::refresh(int force)
{  
   ImlibRenderer::refresh(force);

   imlib_context_set_image(*pImageToDisplay);

   UINT8* frame = (UINT8*)imlib_image_get_data_for_reading_only();
   _iframe = mpeg_draw(frame);

   if (_iframe.data[_iframe.size - 1] != 0xb7) 
   {
      // add sequence end code

      _iframe.data[_iframe.size + 0] = 0x00;
      _iframe.data[_iframe.size + 1] = 0x00;
      _iframe.data[_iframe.size + 2] = 0x01;
      _iframe.data[_iframe.size + 3] = 0xb7;
      _iframe.size += 4;
   }

   _player->setIFrame(_iframe);

   imlib_context_set_image(_cur_image);
}

void DvbRenderer::clear()
{
   ImlibRenderer::clear();
}

//----------------------------------------------------------------------

int DvbRenderer::playVideo(const uchar *Data, int Length)
{
	return _devTo ? _devTo->PlayPes(Data, Length) : 0;
}

bool DvbRenderer::devicePoll(cPoller &Poller, int TimeoutMs)
{
	return _devTo ? _devTo->Poll(Poller, TimeoutMs) : false;
}

void DvbRenderer::setPlayMode(bool Video)
{
	if (_player) _player->setPlayMode(Video);
}

void DvbRenderer::deviceClear(void)
{
	if (_devTo) _devTo->Clear();
}

