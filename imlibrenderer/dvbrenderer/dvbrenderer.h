/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  dvbrenderer.h - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: dvbrenderer.h,v 1.4 2007/11/10 17:46:54 root Exp $
 *
 **/

#ifndef __GTFT_DVBRENDERER_H
#define __GTFT_DVBRENDERER_H

//***************************************************************************
// 
//***************************************************************************

#include <vdr/device.h>

#include <imlibrenderer.h>
#include <player.h>
#include <mpeg2encoder.h>

class cQuantize;

//***************************************************************************
// 
//***************************************************************************

class DvbRenderer : public ImlibRenderer
{
   private:

		int _initialized;
		cGraphTFTPlayer *_player;
		cDevice *_devTo;
		cOsd *_Osd;
		cBitmap *_Bitmap;
		mpeg_frame _iframe;
		cQuantize *_quantizer;
		int _colors;
		int _bpp;

   public:	

		DvbRenderer(int x, int y, int width, int height, string cfgPath, int utf, string thmPath);
		~DvbRenderer();

		int init(int devnum);
		void deinit();

		void refresh(int force = no);
		void clear();

		int playVideo(const uchar *Data, int Length);
		bool devicePoll(cPoller &Poller, int TimeoutMs = 0);
		void setPlayMode(bool Video);
		void deviceClear(void);
};

#endif //__GTFT_DVBRENDERER_H
