/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  imlibrenderer.h - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: imlibrenderer.h,v 1.8 2007/12/03 19:58:05 root Exp $
 *
 **/

#ifndef __GTFT_IMLIBRENDERER_HPP
#define __GTFT_IMLIBRENDERER_HPP

#include "renderer.h"
#include <vdr/device.h>

#define X_DISPLAY_MISSING
#include <Imlib2.h>

#ifdef HAVE_IMAGE_MAGICK
# include <Magick++.h> 
  using namespace Magick; 
#endif

typedef unsigned char UINT8;

//***************************************************************************
// Class ImlibRenderer
//***************************************************************************

class ImlibRenderer : public Renderer
{
	public:

		ImlibRenderer(int x, int y, int width, int height, string cfgPath, int utf, string thmPath);
		virtual ~ImlibRenderer();

      int init(int devnum);
      void flushCache();
      void setFontPath(string fntPath);

      int getImageBuffer(const void*& buffer, int quality);

		virtual void refresh(int force = no);
		virtual void clear();

      int textWidthOf(const char* text, const char* fontName, int fontSize);
      int charWidthOf(const char* fontName = 0, int fontSize = 0);

		void image(const char* fname, int x, int y, int 
                 coverwidth, int coverheight, 
                 bool fit = false, bool aspectRatio = false);
      void imagePart(const char* fname, int x, int y, int width, int height);

		int text(const char* text, const char* font_name, int size, int align, 
               int x, int y, int r, int g, int b, 
               int width, int height,int lines, int dots = 0, int skipLines = 0);

      int lineCount(const char* text, const char* font_name, int size, int width);

		void rectangle(int x, int y, int width, int height, int r, int g, int b, int tr);

		void dumpImage2File(const char* fname, int dumpWidth, int dumpHeight, const char* aPath = 0);

   protected:

		Imlib_Image _cur_image;               // the image you're working on
		Imlib_Image _render_image;            // the image buffer for scaling
		Imlib_Image* pImageToDisplay;         // pointer to the image for displaying

#ifdef HAVE_IMAGE_MAGICK
      Blob blob;
#endif

};

#endif // __GTFT_IMLIBRENDERER_H
