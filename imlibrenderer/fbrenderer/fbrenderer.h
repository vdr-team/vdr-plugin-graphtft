/**
 *  GraphTFT plugin for the Video Disk Recorder 
 * 
 *  fbrenderer.h - A plugin for the Video Disk Recorder
 *
 *  (c) 2004 Lars Tegeler, Sascha Volkenandt  
 *
 * This code is distributed under the terms and conditions of the
 * GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
 *
 * $Id: fbrenderer.h,v 1.3 2007/11/10 09:14:27 root Exp $
 *
 **/

//The most part of this Code is from the MMS V.2 Project:

#ifndef __GTFT_FBRENDERER_HPP
#define __GTFT_FBRENDERER_HPP

#include <imlibrenderer.h>

#ifdef HAVE_SOFTMPEG
#  include <mpeg2decoder.h>
#endif

#include <linux/fb.h>

class FbRenderer : public ImlibRenderer
{

   public:	

		FbRenderer(int x, int y, int width, int height, 
                 string cfgPath, int utf, string thmPath);
		~FbRenderer();

		int init(int devnum);
		void deinit();

		void refresh(int force = no);
		void clear();

		int playVideo(const uchar *Data, int Length);
		bool devicePoll(cPoller &Poller, int TimeoutMs = 0);
		void setPlayMode(bool Video);
		void deviceClear(void);

   private:

		void fbdev_draw_32(unsigned char* frame, int force);
		void fbdev_draw_24(unsigned char* frame, int force);
		void fbdev_draw_16(unsigned char* frame, int force);

      // data

		char* fb_dev_name;
		int _initialized;
		bool _showImage;
		Imlib_Image _resized;

		int fb_dev_fd;
		int fb_type;
		size_t fb_size;
		int fb_line_len;
		int y_offset;

		struct fb_var_screeninfo fb_orig_vinfo;
		struct fb_fix_screeninfo fb_finfo;

#ifdef HAVE_SOFTMPEG
		cMpeg2Decoder* decoder;
#endif

};

#endif // __GTFT_FBRENDERER_H
