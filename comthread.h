//***************************************************************************
// Group VDR/GraphTFT
// File comthread.h
// Date 31.10.06
// This code is distributed under the terms and conditions of the
// GNU GENERAL PUBLIC LICENSE. See the file COPYING for details.
// (c) 2006-2008 J�rg Wendel
//--------------------------------------------------------------------------
// Class ComThread
//***************************************************************************

#ifndef __GTFT_COMTHREAD_HPP__
#define __GTFT_COMTHREAD_HPP__

#include <vdr/plugin.h>
#include <vdr/remote.h>
#include <tcpchannel.h>

#include <renderer.h>
#include <service.h>

//***************************************************************************
// Communication Thread
//***************************************************************************

class ComThread : protected cThread, protected cRemote, public cGraphTftComService
{
   public:
      
      enum Misc
      {
         maxBuf = 512*1024
      };

      ComThread(void* aDisplay, int width, int height);
      virtual ~ComThread();

      void stop()                        { running = false; }
      int refresh();

      const char* getBuffer()            { return buffer; }
      int getSize()                      { return header->size; }

      int init(Renderer* aRenderer, unsigned int aPort = 0, const char* aHost = 0);
      bool Start()                       { return cThread::Start(); }
      void Stop();

      void setHost(const char* aHost)    { strcpy(host, aHost); }
      void setPort(unsigned short aPort) { port = aPort; }
      void setJpegQuality(int value)     { jpgQuality = value; }

   protected:
      
      void Action();
      int read();

      virtual bool Put(uint64_t Code, bool Repeat = false, bool Release = false);

      // data

      TcpChannel* listener;
      Renderer* renderer;
      void* display;
      int themeWidth;
      int themeHeight;

      char* buffer;
      int bufferSize;
      long timeout;
      int running;
      TcpChannel::Header* header;
      unsigned short port;
      char host[100+TB];
      int pid;
      int jpgQuality;

      int clientCount;
      TcpChannel* clients[100];
      time_t lastCheck;
};

//***************************************************************************
#endif // __GTFT_COMTHREAD_HPP__
